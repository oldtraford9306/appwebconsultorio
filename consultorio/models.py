from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class Perfil(models.Model):
    User = models.OneToOneField(User, on_delete = models.CASCADE, primary_key = True)
    nombre = models.CharField(max_length=200)
    edad = models.IntegerField(default=0)
    genero = models.CharField(max_length=20)

