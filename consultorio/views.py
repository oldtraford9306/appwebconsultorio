from django.shortcuts import render
from django.contrib.auth.models import User
from consultorio.models import Perfil
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
# Create your views here.

def home(request):
    return render(request, 'consultorio/index.html')

def perfil(request):
    return render(requert, 'consultorio/perfil.html')

def login(request):
    return render(request, 'consultorio/login.html')

def registro(request):
    return render(request, 'consultorio/registro.html')

def contact(request):
    return render(request, 'consultorio/contact.html')

def registrarUsuario(request):
    if request.method == 'POST':
        user = User.objects.create_user(request.POST.get('usuario'), request.POST.get('correo'), request.POST.get('pass1'))

        perfil = Perfil()

        perfil.User = user
        perfil.nombre = request.POST.get('nombre')
        perfil.edad = request.POST.get('edad')
        perfil.genero = request.POST.get('genero')
        perfil.save()

    return (HttpResponseRedirect('/'))

def auth(request):
    if request.method == 'POST':
        usuario = request.POST.get('usuarioL')
        cont = request.POST.get('passL')
        user = authenticate(request,username=usuario, password=cont)
        if user is not None:
            login (request,user)
            return (HttpResponseRedirect('/home'))
        else:
            return(HttpResponse('Usuario no registrado'))
    else:
            return (HttpResponse('404 Not Found'))

def logout(request):
     do_logout(request)
     return(HttpResponseRedirect('/'))

