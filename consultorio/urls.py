from django.urls import include, path
from . import views



urlpatterns = [
    path('', views.home,name="home"),
    path('login', views.login, name="login"),
    path('registro', views.registro, name="registro"),
    path('contact', views.contact, name="contact"),
    path('registrarUsuario/', views.registrarUsuario, name="registrarUsuario"),
    path('perfil', views.perfil, name="perfil"),
    path('logout/', views.logout, name="logout"),
    path('auth/', views.auth, name = 'auth'),

]